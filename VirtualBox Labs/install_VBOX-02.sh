### install virtualbox + vbox addition
#07/14/2021  
#by @thoutmos

# support.opensource-univesity@ehmou.com 



#trusted  source : 
#https://computingforgeeks.com/install-virtualbox-ubuntu-debian/

# https://wiki.debian.org/VirtualBox#Debian_10_.22Buster.22 



sudo apt update && sudo apt -y upgrade

#Reboot since there could be kernel updates.

#sudo reboot

#Step 2: Import VirtualBox apt repository

#Once the system is updated, import repository GPG key used for signing packages.

sleep 3

sudo apt -y install wget gnupg2   -y

sleep 3


echo " ======= STEP 03 ======= "


wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -

sleep 10

echo " ======= STEP 04 ======= "

wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -

sleep 10






echo " ======= STEP 05 ======= "


# step 3:  Add the VirtualBox Repository

#After the importation of GPG key, add VirtualBox repository to your system 

echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian buster contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list

sleep 10 

#sStep 4: Install VirtualBox & Extension pack

# installation of VirtualBox and extension pack which extends VirtualBox features.

echo " ======= STEP 06 ======= "


sudo apt update
sudo apt install linux-headers-$(uname -r) dkms    -y

sleep 3

sudo apt install virtualbox-6.1 -y


sleep 3

echo " ======= STEP 07 ======= "

echo " ======= TEST ======= "


if ! [ -x "$(command -v virtualbox)" ]; then
  echo 'Error: virtualbox is not installed.' >&2
  exit 1
fi
